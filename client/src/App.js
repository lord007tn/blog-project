import React, {Fragment, useEffect} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import {Provider} from 'react-redux'
import store from './store'
import NavBar from './components/layout/NavBar'
import './App.css';

import Alert from './components/layout/Alert'
import Landing from './components/layout/Landing';
import Login from './components/auth/Login';
import Register from './components/auth/Register'
import Stories from './components/stories/Stories'
import Story from './components/story/Story'

import {loadUser} from './actions/auth'
import setAuthToken from './utils/setAuthToken'
import Dashboard from './components/dashboard/Dashboard';

import PrivateRoute from './components/routing/PrivateRoute'
import Categories from './components/category/Categories';
import StoriesByCategory from './components/stories/StoriesByCategory';
import Footer from './components/footer/Footer';
import AboutUS from './components/about us/AboutUS';
import CreateStory from './components/dashboard/story/CreateStory';
import Subscription from './components/layout/Subscription';
if(localStorage.token){
  setAuthToken(localStorage.token)
}
const App = () => {

  useEffect(()=>{
    store.dispatch(loadUser());
  }, [])
  return (
    <Provider store={store}>
    <Router>
    <Fragment>
    <NavBar />
    <Route exact path= "/" component= {Landing} />
    <Route exact path= "/Subscription" component= {Subscription} />
  <div className="bg-light">
    <Switch>
      <Route exact path="/story/:slug" component= {Story} />
      <Route exact path="/:category/stories" component = {StoriesByCategory} />
      <Route exact path="/about" component = {AboutUS} />
    </Switch>
    <div className= "container">
    <Alert />
      <Switch>
        <Route exact path="/categories" component= {Categories} />
        <Route exact path="/stories" component= {Stories} />
        <PrivateRoute exact path="/register" component= {Register} />
        <Route exact path="/login" component= {Login} />
        <PrivateRoute exact path="/dashboard" component= {Dashboard} />
        <PrivateRoute exact path="/dashboard/stories/story/create" component={CreateStory} />
      </Switch>
    </div>
</div>
    <Footer />
    </Fragment>
    </Router>
    </Provider>
  );
}

export default App;
