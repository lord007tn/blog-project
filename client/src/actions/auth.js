import axios from 'axios';
import {
    REGISTER_FAIL,
    REGISTER_SUCCESS,
    USER_LOADED,
    AUTH_ERROR,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    LOGOUT,
    CLEAR_PROFILE,
    USER_LOADING} from './types'
import {BASE_URL} from '../imgURL'
import setAuthToken from '../utils/setAuthToken'
//Load user

export const loadUser = () => async dispatch => {
    if(localStorage.token){
        setAuthToken(localStorage.token)
    }
    try {
        const res = await axios.get(`${BASE_URL}/authcheck`);
        dispatch({
            type: USER_LOADED,
            payload: res.data
        })
    } catch (err) {
        dispatch({
            type: AUTH_ERROR
        })
    }
}
//Register user

export const register = ({firstName, lastName, userName, email, password}) => async dispatch => {
    dispatch({
        type: USER_LOADING
    })
    const config ={
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const body = JSON.stringify({firstName, lastName, userName, email, password});

    try {
        const res = await axios.post(`${BASE_URL}/register`, body, config)

        dispatch({
            type: REGISTER_SUCCESS,
            payload: res.data
        });
        dispatch(loadUser())
    } catch (err) {
        const errors = err.response.data.errors

        if(err){
            // errors.forEach(err => dispatch(setAlert(err.msg, 'danger')));
        }
        dispatch({
            type: REGISTER_FAIL
        })
    }
}
//login user
export const login = (email, password) => async dispatch => {
    dispatch({
        type: USER_LOADING
    })
    const config ={
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const body = JSON.stringify({email, password});

    try {
        const res = await axios.post(`${BASE_URL}/login`, body, config)

        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data
        });
        dispatch(loadUser())
    } catch (err) {
        const errors = err.response.data.errors

        if(err){
            // errors.forEach(err => dispatch(setAlert(err.msg, 'danger')));
        }
        dispatch({
            type: LOGIN_FAIL
        })
    }
}

export const logout = () => dispatch => {
    dispatch({
        type: USER_LOADING
    })
    dispatch({
        type: CLEAR_PROFILE
    })
    dispatch({
        type: LOGOUT
    })
}