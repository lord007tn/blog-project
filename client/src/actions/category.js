import axios from 'axios';
import {
    CATEGORY_ERROR,
    GET_CATEGORIES,
    GET_CATEGORY
    } from './types'
import {BASE_URL} from '../imgURL'
export const getCategories = () => async dispatch =>{

    try {
        const res = await axios.get(`${BASE_URL}/category/categories`);
        dispatch({
            type: GET_CATEGORIES,
            payload: res.data
        })
    } catch (err) {
        dispatch({
            type: CATEGORY_ERROR,
            payload: err
        });
    }

}

export const getCategory = (name) => async dispatch =>{

    try {
        const res = await axios.get(`${BASE_URL}/category/${name}`);
        dispatch({
            type: GET_CATEGORY,
            payload: res.data
        })
    } catch (err) {
        dispatch({
            type: CATEGORY_ERROR,
            payload: err
        });
    }
}