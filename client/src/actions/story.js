import axios from 'axios';
import {setAlert} from './alert'
import {GET_STORIES,
    STORY_ERROR,
    DELETE_STORY,
    GET_STORY,
    ADD_COMMENT,
    REMOVE_COMMENT,
    COMMENT_ERROR,
    GET_COMMENTS,
    CREATE_STORY} from './types'
import {BASE_URL} from '../imgURL'
//Get stories
export const getStories = ()=> async dispatch =>{
    try {
        const res = await axios.get(`${BASE_URL}/story/stories`);
        dispatch({
            type: GET_STORIES,
            payload: res.data
        });

    } catch (err) {
        dispatch({
            type: STORY_ERROR,
            payload: err
        });
    }

}
export const getStory = (slug)=> async dispatch =>{
    try {
        const res = await axios.get(`${BASE_URL}/story/${slug}`);
        dispatch({
            type: GET_STORY,
            payload: res.data
        });

    } catch (err) {
        dispatch({
            type: STORY_ERROR,
            payload: err
        });
    }

}

export const createStory = ({title, body, description, tagList}, author) => async dispatch =>{

    const config ={
        headers:{
            'Content-Type': 'application/json'
        }
    }
    const story = JSON.stringify({title, body, description, author, tagList});
    try {
        const res = await axios.post(`${BASE_URL}/story/create`, story, config)
        dispatch({
            type: CREATE_STORY,
            payload: res.data
        })
    } catch (err) {
        dispatch({
            type: STORY_ERROR,
            payload: err
        });
    }
}



export const deleteStory = (slug) => async dispatch =>{

    try {
        const res = await axios.delete(`${BASE_URL}/story/${slug}/delete`);

        dispatch({
            type: DELETE_STORY,
            payload: slug
        })

        dispatch(setAlert('Story removed', "success"))
    } catch (err) {
        dispatch({
            type: STORY_ERROR,
            payload: err
        });
    }
}

export const getComments = (slug)=> async dispatch =>{
    try {
        const res = await axios.get(`${BASE_URL}/story/${slug}/comments`);
        dispatch({
            type: GET_COMMENTS,
            payload: res.data
        });

    } catch (err) {
        dispatch({
            type: COMMENT_ERROR,
            payload: err
        });
    }

}

export const createComment = (storySlug, FormData) => async dispatch => {
    const config ={
        headers:{
            'Content-Type': 'application/json'
        }
    }

    try {
        const res = await axios.post(`${BASE_URL}/story/${storySlug}/comment/create`, FormData, config);
        dispatch({
            type: ADD_COMMENT,
            payload: res.data
        })
        dispatch(setAlert('Comment Added', 'success'))
    } catch (err) {
        dispatch({
            type: COMMENT_ERROR,
            payload: err
        })
    }
}
export const deleteComment = (storySlug, commentID) => async dispatch => {

    try {
        const res = await axios.delete(`${BASE_URL}/story/${storySlug}/comment/${commentID}/delete`);
        dispatch({
            type: REMOVE_COMMENT,
            payload: commentID
        })
        dispatch(setAlert('Comment Added', 'danger'))
    } catch (err) {
        dispatch({
            type: COMMENT_ERROR,
            payload: err
        })
    }
}