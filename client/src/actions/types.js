export const USER_LOADING = 'USER_LOADING'
//Alert
export const SET_ALERT = 'SET_ALERT'
export const REMOVE_ALERT = 'REMOVE_ALERT'
//User auth
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAIL = 'REGISTER_FAIL'
export const USER_LOADED = 'USER_LOADED'
export const AUTH_ERROR = 'AUTH_ERROR'
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGOUT = 'LOGOUT'
//User Profile
export const GET_PROFILE = 'GET_PROFILE'
export const PROFILE_ERROR = 'PROFILE_ERROR'
export const CLEAR_PROFILE = 'CLEAR_PROFILE'
//Stories
export const GET_STORIES = 'GET_STORIES'
export const GET_STORY = 'GET_STORY'
export const STORY_ERROR = 'STORY_ERROR'
export const UPDATE_LOVES_COUNT = 'UPDATE_LOVES_COUNT'
export const DELETE_STORY = 'DELETE_STORY'
export const CREATE_STORY = 'CREATE_STORY'
export const EDIT_STORY = 'EDIT_STORY'
//Stories comments
export const GET_COMMENTS = 'GET_COMMENTS'
export const ADD_COMMENT = 'ADD_COMMENT'
export const REMOVE_COMMENT = 'REMOVE_COMMENT'
export const COMMENT_ERROR = 'COMMENT_ERROR'

//Categories
export const GET_CATEGORIES = 'GET_CATEGORIES'
export const CATEGORY_ERROR = 'CATEGORY_ERROR'
export const GET_CATEGORY = 'GET_CATEGORY'
