import React from 'react'
import PropTypes from 'prop-types'
import { Fragment } from 'react'
import LandingSubscription from '../layout/LandingSubscription'

import notschool_about from '../../static/img/notschool_about.png'
import avatar from '../../static/img/avatar.png'
const AboutUS = props => {
    return (
        <Fragment>
            <div className="site-section bg-light">
      <div className="container">
        <div className="row">
          <div className="col-md-6 order-md-2">
            <img src={notschool_about} alt="Image" className="img-fluid"/>
          </div>
          <div className="col-md-5 mr-auto order-md-1">
            <h2>Explore ...</h2>
            <p> We are not a school, So remember you are welcome and no one is forcing you to stay </p>
          </div>
        </div>
      </div>
    </div>
    <div className="site-section">
      <div className="container">
        <div className="row mb-5 justify-content-center">
          <div className="col-md-5 text-center">
            <h2>Meet The Team</h2>
            <p>People who are suffering to make this text block centered</p>
          </div>
        </div>
        <div className="row">

          <div className="col-md-6 offset-md-3 text-center">
            <img src={avatar} alt="Image" className="img-fluid w-50 rounded-circle mb-4" style={{margin:"0px auto"}}/>
            <h2 className="mb-3 h5">Raed Bahri</h2>
            <p>I love opening a new Chrome window with dozens of research tabs, and getting too stressful when the tabs names start to hide, just to fix one issue. I'm not a programmer, nor an expert on the stuff I write about, I'm just a professional Googler and a guy who seem to know what code to copy from StackOverFlow.</p>

            <p className="text-center">
                  <a href="https://www.facebook.com/raedbahri90" target="_blank" className="p-3" style={{color:"#AB382F"}}><i className="fab fa-facebook-square"></i></a>
                  <a href="https://www.linkedin.com/in/raed-bahri-738133172/" target="_blank" className="p-3" style={{color:"#AB382F"}}><i className="fab fa-linkedin"></i></a>
                  <a href="mailto:raedbahri90@gmail.com" target="_blank" className="p-3" style={{color:"#AB382F"}}><i className="fas fa-envelope"></i></a>
            </p>
          </div>

        </div>
      </div>
    </div>
        <LandingSubscription />
        </Fragment>
    )
}

AboutUS.propTypes = {

}

export default AboutUS
