import React, { Fragment, useState } from 'react'
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { login } from "../../actions/auth";
import { Redirect, Link } from 'react-router-dom';

import Spinner from '../layout/Spinner'
const Login = ({login, isAuthenticated, loading}) => {
    const [FormData, setFormData] = useState({
        email:'',
        password:'',
    });
    
    const {email, password} = FormData;
    const onChange = e => setFormData({...FormData, [e.target.name]: e.target.value})
    const onSubmit = e =>{
       e.preventDefault()
       login(email, password);
       setFormData({email:"", password:""})
    }

    //redirect if logged in
    if(isAuthenticated){
        return <Redirect to='/dashboard' />
    }
    return (
        loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) : (
        <Fragment>
        <div className= "card-wrapper">
            <div className="card card-2">
            <div className="card-heading"></div>
            <div className="card-body">
            <h5 className="title">Login</h5>
            <form onSubmit={ e=> onSubmit(e)}>
            <div className="input-group">
                <input type="email" className="input-form" name="email" value= {email} onChange={e => onChange(e)} placeholder="Email" required/>
            </div>
            <div className="input-group">
                <input type="password" className="input-form" name="password" value= {password} onChange={e => onChange(e)} placeholder="Password" required/>
            </div>
            <div className="row">
                <div className="col-4">
                    <button className="btn btn--radius btn--color" type="submit">Login</button>
                </div>
            </div>

            </form>
            </div>
        </div>
    </div>
        </Fragment>
    )

    )
}

Login.propTypes = {
    login: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
    loading: PropTypes.bool.isRequired,
}
const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    loading: state.auth.loading
})

export default connect(mapStateToProps, {login})(Login)
