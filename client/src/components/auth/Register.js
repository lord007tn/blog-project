import React, { Fragment, useState } from 'react'
import {connect} from 'react-redux'
import { setAlert } from "../../actions/alert";
import {register} from '../../actions/auth';
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom';

import Spinner from '../layout/Spinner'
const Register = ({ setAlert, register, isAuthenticated, loading}) => {
    const [FormData, setFormData] = useState({
        firstName: '',
        lastName: '',
        userName:'',
        email:'',
        password:'',
        password2:''
    });
    const {firstName, lastName, userName, email, password, password2} = FormData;
    const onChange = e => setFormData({...FormData, [e.target.name]: e.target.value})
    const onSubmit = e =>{
       e.preventDefault()
       if(password !== password2){
           setAlert('Password dont match', 'danger')
           setFormData({password:"", password2:""})
       }else{
            register({firstName, lastName, userName, email, password});
            setFormData({userName:"", email:"", password:"", password2:""})
       }
    }
    //redirect if logged in
    if(isAuthenticated){
        return <Redirect to="/dashboard" />
    }
    return (loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) : (
        <Fragment>
        <div className= "card-wrapper">
            <div className="card card-2">
            <div className="card-heading"></div>
            <div className="card-body">
            <h5 className="title">REGISTRATION INFO</h5>
            <form onSubmit={ e=> onSubmit(e)}>
            <div className="row">
            <div className= "col">
            <div className="input-group">
                <input type="text" className="input-form" name="firstName" value= {firstName} onChange={e => onChange(e)}  placeholder="First Name" required/>
            </div>
            </div>
            <div className="col">
            <div className="input-group">
                <input type="text" className="input-form" name="lastName" value= {lastName} onChange={e => onChange(e)} placeholder="Last Name" required/>
            </div>
            </div>

            </div>
            <div className="input-group">
                <input type="text" className="input-form" name="userName" value= {userName} onChange={e => onChange(e)} placeholder="Username" required/>
            </div>
            <div className="input-group">
                <input type="email" className="input-form" name="email" value= {email} onChange={e => onChange(e)} placeholder="Email" required/>
            </div>
            <div className="input-group">
                <input type="password" className="input-form" name="password" value= {password} onChange={e => onChange(e)} placeholder="Password" required/>
            </div>
            <div className="input-group">
                <input type="password" className="input-form" name="password2" value= {password2} onChange={e => onChange(e)} placeholder="Confirm Password" required/>
            </div>
            <div className="row">
            <div className="col-sm-8">
            <div className="input-group">
                <input type="text" className="input-form" placeholder="Registration Code"/>
            </div>
            </div>
            </div>
            <div className="row">
                <div className="col-4">
                    <button className="btn btn--radius btn--color" type="submit">Register</button>
                </div>
            </div>

            </form>
            </div>
        </div>
    </div>
        </Fragment>
    )
    )
}
Register.propTypes = {
    setAlert: PropTypes.func.isRequired,
    register: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
    loading: PropTypes.bool.isRequired,
}
const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    loading: state.auth.loading
})
export default connect(mapStateToProps, {setAlert, register})(Register)
