import React, {Fragment, useEffect} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import { getCategories } from '../../actions/category'
import Spinner from '../layout/Spinner'
import CategoryItem from './CategoryItem'
const Categories = ({category:{categories, loading}, getCategories}) => {
    useEffect(()=>{
        getCategories();
    }, [getCategories])


    return (loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) :<Fragment>
    <div className="site-section">
        <div className="row mb-5">
          <div className="col-12">
            <h2>Explore stories by category</h2>
          </div>
        </div>
        <div className="row">
        {categories.map(category=>(
            <CategoryItem key={category._id} category= {category} />
        ))}
</div>
        </div>
        </Fragment>
    )
}

Categories.propTypes = {
    category: PropTypes.object.isRequired,
    getCategories: PropTypes.func.isRequired,
}
const mapStateToProps = state =>({
    category: state.category

})
export default connect(mapStateToProps, {getCategories})(Categories)
