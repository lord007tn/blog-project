import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import { Link } from 'react-router-dom'
const CategoryItem = ({category}) => {
    return (
        <Fragment>
        <div className= "col-lg-4 mb-4">
        <Link to={`/${category.category}/stories`}>
        <div className="card card-2-category">
            <div className="card-heading-category" style={{background: `#${category.color}`}}></div>
            <div className="card-body-category">
            <h5 className="title-category">{category.category}</h5>
            <p className="about-category">{category.about}</p>
            </div>
        </div>
        </Link>
        </div>
        </Fragment>
    )
}

CategoryItem.propTypes = {
    category: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
})
export default connect(mapStateToProps, {})(CategoryItem)
