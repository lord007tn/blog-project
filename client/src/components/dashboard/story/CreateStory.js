import React, {Fragment, useState} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Spinner from '../../layout/Spinner'
import { createStory } from '../../../actions/story'
const CreateStory = ({loading, user, createStory}) => {
    
    const [FormData, setFormData] = useState({
		title: "",
		body: "",
        description: "",
        tag:"",
        tagList: [],
    });
    const {title, body, description, author, tag, tagList} = FormData;
    const onChange = e => setFormData({...FormData, [e.target.name]: e.target.value})
    const onSubmit = e =>{
            e.preventDefault()
            createStory({title, body, description, tagList}, user._id)
            setFormData({title:"", body:"", description:"", tag:"", tagList:[]})
    }
    const pushTag = e =>{
        if(e.keyCode === 13 && e.target.value){
            tagList.push(e.target.value)
            e.target.value = ""
        }
    }
    return (loading && user === null ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) :
        <Fragment>
        <div className= "card-wrapper">
            <div className="card card-2">
            <div className="card-body">
            <h5 className="title">Create Story</h5>
            <form >
            <div className="row">
            <div className="input-group">
                <input type="text" className="input-form" name="title" value= {title} onChange={e => onChange(e)}  placeholder="Title" />
            </div>
            </div>
            <div className="row">
            <div className="input-group">
                <textarea type="text" className="input-form" name="body" value= {body} onChange={e => onChange(e)}  placeholder="Story Body" rows="7"  />
            </div>
            </div>
            <div className="row">
            <div className="input-group">
                <textarea type="text" className="input-form" name="description" value= {description} onChange={e => onChange(e)} placeholder="Story Description" rows="3" />
            </div>
            </div>
            <div className="row">
            <div className="input-group">
            {tagList !== undefined && tagList.map(tag =>(
                <span key={Math.random()} className="label-tag badge-primary">{tag}</span>
            ))}
                <input type="text" className="input-form" name="tag" value= {tag} onChange={e => onChange(e)} onKeyUp={e => pushTag(e)} placeholder="Tags" />
            </div>
            </div>
            <div className="row">
                    <button onClick={ e=> onSubmit(e)} className="btn btn--radius btn--color" type="button">Create Story</button>
            </div>

            </form>
            </div>
        </div>
    </div>
        </Fragment>
    )
}

CreateStory.propTypes = {
    user: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    createStory: PropTypes.func.isRequired,
}
const mapStateToProps = state => ({
    user:state.auth.user,
    loading: state.auth.loading,
})
export default connect(mapStateToProps, {createStory})(CreateStory)
