import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
const Footer = props => {
    return (
        <Fragment>
<div className="site-footer">
        <div className="container">
          <div className="row mb-5">
            <div className="col-md-4">
              <h3 className="footer-heading mb-4">About Us</h3>
              <p>We are not a school, So remember you are welcome and no one is forcing you to stay</p>
            </div>
            <div className="col-md-3 ml-auto">
             <h3 className="footer-heading mb-4">Navigation</h3>
              <ul className="list-unstyled float-left mr-5">
                <li><Link to="/about">About Us</Link></li>
                <li><Link to="/stories">Stories</Link></li>
                <li><Link to="/categories">Categories</Link></li>
                <li><Link to="/">Home</Link></li>
              </ul>
              <ul className="list-unstyled float-left">
                <li><Link to="/startups/stories">Startups</Link></li>
                <li><Link to="/life style/stories">Lifestyle</Link></li>
                <li><Link to="/science/stories">Science</Link></li>
                <li><Link to="/nature/stories">Nature</Link></li>
              </ul>
            </div>
            <div className="col-md-4">
              
  
              <div>
                <h3 className="footer-heading mb-4">Connect With Us</h3>
                <p>
                  <a href="#" target="_blank"><i className="fab fa-facebook-square p-2 pr-2"></i></a>
                  <a href="#" target="_blank"><i className="fab fa-twitter-square p-2 pr-2"></i></a>
                  <a href="#" target="_blank"><i className="fab fa-instagram-square p-2 pr-2"></i></a>
                  <a href="mailto:raedbahri90@gmail.com" target="_blank"><i className="fas fa-envelope p-2 pr-2"></i></a>
                </p>
              </div>
            </div>
          </div>
          <div className="row">
          <div className="col-12 text-center">
            <p>
              Copyright &copy; {new Date().getFullYear()} All rights reserved | By <Link to="/" target="_blank" >NotSchoolHut</Link>
              </p>
          </div>
        </div>
        </div>
      </div>
        </Fragment>

    )
}

Footer.propTypes = {

}

export default Footer
