import React, { Fragment, useEffect } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import LandingSubscription from './LandingSubscription'
import LandingStoryItem from './LandingStoryItem'
import { getStories } from '../../actions/story'
import Spinner from './Spinner'
const Landing = ({isAuthenticated, story:{stories, loading}, getStories}) => {
  useEffect(()=>{
    getStories();
}, [getStories])

  return (
      loading ? (<div className="disableAllWithSpin">
      <div className="centerSpin">
          <Spinner />
      </div>
      </div>) : (
        <Fragment>
        <LandingSubscription />
  <div className="site-section bg-light">
    <div className="container">
      <div className="row retro-layout-2">
      {stories.map(story => (
        <LandingStoryItem key={story._id} story={story} />
      ))}
      </div>
    </div>
  </div>
</Fragment>
      )

    )
}

Landing.propTypes = {
  isAuthenticated: PropTypes.bool,
  story: PropTypes.object.isRequired,
  getStories: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  auth: state.auth,
  story: state.story
})
export default connect(mapStateToProps, {getStories})(Landing)
