import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Moment from 'react-moment'
import {BASE_IMG_URL} from '../../imgURL'
const LandingStoryItem = ({story}) => {
    return (
        <Fragment>
          <div className="col-md-4">
            <div >
            <Link to={`/story/${story.slug}`} className="h-entry mb-30 v-height gradient" style={{backgroundImage: `url(http://${BASE_IMG_URL}/image/${story.storyImage})`}}></Link>
              <div className="text">
                <div className="post-categories">
                {story.category.map(cat=>(
                    <Link key={cat._id} to={`/${cat.category}/stories`}>
                    <span className="post-category text-white mb-3" style={{background: `#${cat.color}`, margin:"0 2px"}}>{cat.category}</span>
                    </Link>
                ))}
                </div>
                <Link to={`/story/${story.slug}`}><h2 className="text-capitalize">{story.title}</h2></Link>
                <span className="date"><Moment format='MMMM Do YYYY'>{story.createdAt}</Moment></span>
              </div>
            </div>
          </div>
        </Fragment>
    )
}

LandingStoryItem.propTypes = {
    story: PropTypes.object.isRequired,
}

const mapStateToProps = state =>({
})
export default connect(mapStateToProps, {})(LandingStoryItem)
