import React, {useState} from 'react'
import PropTypes from 'prop-types'

const LandingSubscription = props => {
  const [FormData, setFormData] = useState({MERGE0:''});
  const {MERGE0} = FormData;
  const onChange = e => setFormData({...FormData, [e.target.name]: e.target.value})
    return (
<div className="bg-sub">
<div className="site-section bg-lightx">
      <div className="container">
        <div className="row justify-content-center text-center">
          <div className="col-md-8">
            <div className="subscribe-1 ">
              <h1>What's <span className="font-weight-bold">knowledge</span> beyond school walls?</h1>
              <p className="mb-5">Hey, I'm here just like you suffering from the educational system. Follow my journey far from the school box.</p>
              <form className="d-flex" action="https://digital.us8.list-manage.com/subscribe/post" method="post">
              <input type="hidden" name="u" value="572339b8940a83433c14153ad"/>
              <input type="hidden" name="id" value="5bd96b79a4"/>
              <input type="hidden" name="c" value="?" />
              <div className="input-group">
                <input type="email" className="input-form" name="MERGE0" id="MERGE0" value= {MERGE0} onChange={e => onChange(e)} placeholder="Your Email" required/>
            </div>
                    <button className="btn btn--sub btn--radius btn--color--sub" type="submit" target="_blank">Subscribe</button>
              </form>
              <p style={{marginTop:"3rem"}} >
              <em >By entering your email address you agree to get email updates from NotSchoolHut. I'll respect your privacy and you can unsubscribe at any time.</em>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    )
}

LandingSubscription.propTypes = {

}

export default LandingSubscription
