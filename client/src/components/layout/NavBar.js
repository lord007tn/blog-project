import React, { Fragment } from 'react'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {logout} from '../../actions/auth'
const NavBar = ({logout, auth: {isAuthenticated, loading}}) => {

  const authLinks = (
    <ul className="site-menu js-clone-nav mr-auto d-none d-lg-block mb-0" style={{fontSize: "1rem"}}>
    <li><Link to="/">Home</Link></li>
    <li><Link to="/stories">Stories</Link></li>
    <li><Link to="/categories">Categories</Link></li>
    <li><Link to="/about">About</Link></li>
    <li><Link to="/dashboard">Dashboard</Link></li>
    <li><Link onClick={logout} to="/">Logout</Link></li>
    
  </ul>
  )

  const guestLinks = (
    <ul className="site-menu js-clone-nav mr-auto d-none d-lg-block mb-0" style={{fontSize: "1rem"}}>
    <li><Link to="/">Home</Link></li>
    <li><Link to="/stories">Stories</Link></li>
    <li><Link to="/categories">Categories</Link></li>
    <li><Link to="/about">About</Link></li>
    {/* <li><Link to="/register">Register</Link></li> */}
    <li><Link to="/login">Login</Link></li>
  </ul>
  )
    return (
      <div>
  <header className="site-navbar" role="banner">
      <div className="container-fluid">
        <div className="row align-items-center">
          <div className="col-12 search-form-wrap js-search-form">
          </div>

          <div className="col-4 site-logo">
            <Link to="/" ><p className="text-black h2 mb-0" style={{fontFamily:"Poppins"}}><span className="h2 mb-0 font-weight-bold" style={{color:"#AB382F", fontFamily:"Poppins"}}>Not</span>SchoolHut</p></Link>
          </div>

          <div className="col-8 text-right">
            <nav className="site-navigation" role="navigation">
              {!loading && (<Fragment>{isAuthenticated ? authLinks: guestLinks}</Fragment>)}
            </nav>
            <a href="#" className="site-menu-toggle js-menu-toggle text-black d-inline-block d-lg-none"><span className="icon-menu h3"></span></a></div>
          </div>

      </div>
    </header>
    </div>
    )
}
NavBar.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = state =>({
  auth: state.auth,
})
export default connect(mapStateToProps, {logout})(NavBar)
