import React, {useEffect, Fragment} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {getStories} from '../../actions/story'
import StoryItem from './StoryItem'
import Spinner from '../layout/Spinner'
const Stories = ({getStories, story: {stories, loading}}) => {

    useEffect(()=>{
        getStories();
    }, [getStories])
    return ( loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) : <Fragment>
    <div className="site-section">

    
        <div className="row mb-5">
          <div className="col-12">
            <h2>Stories</h2>
          </div>
        </div>
        <div className="row">
            {stories.map(story=>(
                <StoryItem key={story._id} story={story} />
            ))}
            </div>
        </div>
    </Fragment>
    
    )
}

Stories.propTypes = {
    getStories: PropTypes.func.isRequired,
    story: PropTypes.object.isRequired,
}
const mapStateToProps = state => ({
    story: state.story
})

export default connect(mapStateToProps, {getStories})(Stories)
