import React, { Fragment, useEffect } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import { Link } from 'react-router-dom'
import Spinner from '../layout/Spinner'
import { getCategory } from '../../actions/category'
import { getStories } from '../../actions/story'
import StoryItem from './StoryItem'
const StoriesByCategory = ({story:{stories, loading}, category:{category}, match, getCategory, getStories}) => {

    const storyByCategory = stories.filter(story => story.category.some(cat=> cat.category === match.params.category))
    useEffect(()=>{
        getCategory(match.params.category);
        getStories();
    },[getCategory, getStories, match.params.category])
    return (
        loading ?
    (<div className="disableAllWithSpin">
     <div className="centerSpin">
        <Spinner />
     </div>
     </div>) : (
        <Fragment>
    <div className="py-5 bg-light">
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <span>Category</span>
            <h3 className="text-capitalize">{match.params.category}</h3>
            {category !== null && <p>{category.about}</p>}
          </div>
        </div>
      </div>
    </div>
    <div className="container">
    <div className="row">
        {storyByCategory.map(story=>(
                <StoryItem key={story._id} story={story} />
            ))}
        </div>
    </div>

        </Fragment>
     )

    )
}

StoriesByCategory.propTypes = {
    story: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired,
    getCategory: PropTypes.func.isRequired,
    getStories: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
    story: state.story,
    category: state.category,
})
export default connect(mapStateToProps, {getCategory, getStories})(StoriesByCategory)
