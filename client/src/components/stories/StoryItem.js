import React, {Fragment} from 'react'
import PropTypes from 'prop-types'

import {Link} from 'react-router-dom'
import Moment from 'react-moment'
import {connect} from 'react-redux'
import {deleteStory} from '../../actions/story'
import {BASE_IMG_URL} from '../../imgURL'
const StoryItem = ({story, auth, deleteStory}) => {
    return (
      <Fragment>
      <div className="col-12 col-sm-8 col-md-6 col-lg-4" style={{paddingBottom: "15px", paddingTop:"15px"}} >
        <div className="card">
        <div className="site-cover site-cover-sm same-height overlay single-page" style={{backgroundImage: `url(http://${BASE_IMG_URL}/image/${story.storyImage})`}}></div>
          <div className="card-img-overlay" style={{zIndex: 0}} >
          {story.category.map(cat=>(
            <Link key={cat._id} to={`/${cat.category}/stories`}>
             <span className="post-category text-white mb-3" style={{backgroundColor:`#${cat.color}`, margin:"0 2px"}}>{cat.category}</span>
              </Link>
          ))}
            
          </div>
          <div className="card-body" style={{zIndex: 1}}>
            <Link to={`/story/${story.slug}`}><h4 className="card-title text-capitalize">{story.title}</h4></Link>
            <small className="text-muted cat">
              <i className="far fa-clock" style={{color:"#1f1f1f"}}></i> {story.readingTime} minutes
            </small>
            <p className="card-text">{story.description}...</p>
              <Link to={`/story/${story.slug}`} className="btn btn--color">Read More</Link>
          </div>
          <div className="card-footer text-muted d-flex justify-content-between bg-transparent border-top-0">
            <div className="views" style={{color:"#1f1f1f"}}><Moment format='MMMM Do YYYY'>{story.createdAt}</Moment>
            </div>
            <div className="stats">
              <i className="fas fa-heart" style={{color:"#1f1f1f"}}></i> {story.lovesCount}
              <span style={{margin: "0 5px"}}> </span>
              <i className="far fa-comment" style={{color:"#1f1f1f"}}></i> {story.comments.length}
            </div>
          </div>
        </div>
        {!auth.loading && auth.isAuthenticated && (<div className="row">
                <div className="col-4">
                    <button className="btn btn--radius btn--color" type="button" onClick={e => deleteStory(story.slug)}><i className="fas fa-trash-alt"></i></button>
                </div>
            </div>) }
      </div>
      </Fragment>
    )

}

StoryItem.propTypes = {
    story: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    deleteStory: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
    auth: state.auth
})

export default connect(mapStateToProps, {deleteStory})(StoryItem)
