import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {BASE_IMG_URL} from '../../imgURL'
import {Link} from 'react-router-dom'
const Author = ({story: {story}}) => 
    story !== null && story.author !== null && (
        <Fragment>
        <div className="sidebar-box">
        <div className="bio text-center">
          <img src={`http://${BASE_IMG_URL}/image/${story.author.avatar}`} alt= "avatar" className="img-fluid mb-5 mt-5"/>
          <div className="bio-body">
            <h2 className="text-capitalize">{story.author.firstName} {story.author.lastName}</h2>
            <p className="mb-4 mr-3 ml-3 text-left">{story.author.bio}</p>
            <p><Link to="#" className="btn btn--sub btn--radius btn--color--sub px-4 py-2">Read my bio</Link></p>
            <p className="social">
            <a href="https://www.facebook.com/raedbahri90" target="_blank" className="p-3" style={{color:"#AB382F"}}><i className="fab fa-facebook-square"></i></a>
            <a href="https://www.linkedin.com/in/raed-bahri-738133172/" target="_blank" className="p-3" style={{color:"#AB382F"}}><i className="fab fa-linkedin"></i></a>
            <a href="mailto:raedbahri90@gmail.com" target="_blank" className="p-3" style={{color:"#AB382F"}}><i className="fas fa-envelope"></i></a>
            </p>
          </div>
        </div>
      </div>
        </Fragment>
    )

Author.propTypes = {
    story: PropTypes.object.isRequired,
}
const mapStateToProps = state =>({
    story: state.story,
})
export default connect(mapStateToProps, {})(Author)
