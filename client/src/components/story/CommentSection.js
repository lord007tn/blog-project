import React, {useEffect, Fragment} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Moment from 'react-moment'
import {getComments} from '../../actions/story'
import {BASE_IMG_URL} from '../../imgURL'
const CommentSection = ({story: {story, comments}, getComments}) => {
    useEffect(()=>{
        getComments(story.slug)
    }, [getComments,story.slug])

    return (story !== null && comments !== null && (
        <Fragment>
            <h3 className="mb-5">Comments</h3>
              <ul className="comment-list">
              {comments.map(comment =>(
                <li key={comment._id} className="comment">
                  <div className="vcard">
                    <img src={`http://${BASE_IMG_URL}/image/${comment.author.avatar}`} alt="avatar"/>
                  </div>
                  <div className="comment-body">
                    <h3 className="text-capitalize">{comment.author.firstName} {comment.author.lastName}</h3>
                    <div className="meta"><Moment format='MMMM Do YYYY, mm:hh'>{comment.createdAt}</Moment></div>
                    <p>{comment.body}</p>
                    {/* <p><a href="#" className="reply rounded">Reply</a></p> */}
                  </div>
                </li>
              ))}

             </ul>
        </Fragment>
    )
    )
}

CommentSection.propTypes = {
    story: PropTypes.object.isRequired,
    getComments: PropTypes.func.isRequired,

}
const mapStateToProps = state => ({
  story: state.story
})

export default connect(mapStateToProps, {getComments})(CommentSection)
