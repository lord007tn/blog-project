import React, {Fragment, useEffect} from 'react'
import PropTypes from 'prop-types'

import {connect} from 'react-redux'
import {getStory} from '../../actions/story'
import Spinner from '../layout/Spinner'
import StoryCarousel from './StoryCarousel'
import StoryContent from './StoryContent'
import CommentSection from './CommentSection'
import Author from './Author'
import TagsSection from './TagsSection'

const Story = ({getStory, story : {story, loading}, match}) => {
    useEffect(()=>{
        getStory(match.params.slug)
    }, [getStory, match.params.slug])
    return ( story === null && loading  ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) : (<Fragment>
    
    <StoryCarousel/>
        <section className="site-section py-lg">
        <div className="container">
        <div className="row blog-entries element-animate">
        <div className="col-md-12 col-lg-8 main-content">
            <StoryContent/>
        <div className="pt-5">
            {story !==null &&(<CommentSection story={story}/>)}
        </div>
            
        </div>
        <div className="col-md-12 col-lg-4 sidebar">
            <Author/>
            <TagsSection/>
        </div>
    </div>
</div>
    </section>

    </Fragment>)
        
    )
}

Story.propTypes = {
    getStory: PropTypes.func.isRequired,
    story: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
    story: state.story
})
export default connect(mapStateToProps, {getStory})(Story)
