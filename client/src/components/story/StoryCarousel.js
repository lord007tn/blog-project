import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Moment from 'react-moment'
import {BASE_IMG_URL} from '../../imgURL'
import { Link } from 'react-router-dom'
const StoryCarousel = ({story: {story}}) => 
    story !== null &&(
        <Fragment>
        <div className="site-cover site-cover-sm same-height overlay single-page" style={{backgroundImage: `url(http://${BASE_IMG_URL}/image/${story.storyImage})`}}>
        <div className="container">
          <div className="row same-height justify-content-center">
            <div className="col-md-12 col-lg-10">
              <div className="post-entry text-center">
              {story.category.map(cat => (
                <Link key={cat._id} to={`/${cat.category}/stories`}>
                <span  className="post-category text-white mb-3" style={{backgroundColor:`#${cat.color}`, margin:"0 2px"}}>{cat.category}</span>
                </Link>
                
              ))}
                <h1 className="mb-4 text-capitalize">{story.title}</h1>
                <div className="post-meta align-items-center text-center">
                  <figure className="author-figure mb-0 mr-3 d-inline-block"><img src={`http://${BASE_IMG_URL}/image/${story.author.avatar}`} alt="avatar" className="img-fluid"/></figure>
                  <span className="d-inline-block mt-1 text-capitalize">{story.author.firstName} {story.author.lastName}</span>
                  <span>&nbsp;-&nbsp; <Moment format='MMMM Do YYYY'>{story.createdAt}</Moment></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        </Fragment>
    )



StoryCarousel.propTypes = {
    story: PropTypes.object.isRequired,
}

const mapStateToProps = state =>({
    story: state.story
})
export default connect(mapStateToProps, {})(StoryCarousel)
