import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Markdown from 'markdown-to-jsx';
import SEO from '../../utils/SEO'

import {BASE_IMG_URL} from '../../imgURL'
const StoryContent = ({story: {story}}) => {

    return (
        story !== null &&
        (<Fragment>
        <SEO pageProps={{title: story.title, description: story.description, slug: story.slug}}/>
        <div className="post-content-body">
        <Markdown>
        {story.sanitizedHtml}
        </Markdown>
            
        </div>
        </Fragment>)
    )
}
    



StoryContent.propTypes = {
    story: PropTypes.object.isRequired,
}
const mapStateToProps = state =>({
    story: state.story
})
export default connect(mapStateToProps, {})(StoryContent)
