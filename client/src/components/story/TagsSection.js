import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
const TagsSection = ({story:{story}}) =>
    story !== null && story.tagList !== null && (
        <Fragment>
            <div className="sidebar-box">
              <h3 className="heading">Tags</h3>
              <ul className="tags">
              {story.tagList.map(tag=>(
                <li key={tag._id}><Link to="#">{tag.tag}</Link></li>
              ))}
              </ul>
            </div>
        </Fragment>
    )



TagsSection.propTypes = {
    story: PropTypes.object.isRequired,
}
const mapStateToProps = state => ({
    story: state.story
})
export default connect(mapStateToProps, {})(TagsSection)
