import React from 'react';
import {Helmet} from "react-helmet";
import sub from '../static/img/Subscription.jpg'
function SEO({ pageProps }) {
  return (
    <Helmet>
        <title>{pageProps.title}</title>
        <meta property="og:title" content={pageProps.title || "NotSchoolHut"} />
        <meta property="og:description" content={pageProps.description || "NotSchoolHut Blog"} />
        <meta property="og:url" content={`http://notschoolhut.digital/story/${pageProps.slug}`} />
    </Helmet>
  )
}

export default SEO;