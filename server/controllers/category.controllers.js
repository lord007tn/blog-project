const Category = require( "../models/category.models" );

const createCategory = async( req, res )=>{

	const category = new Category( {
		category: req.body.category,
		color: req.body.color,
		about: req.body.about
	} );

	try {
		const savedCategory = await category.save();
		return res.json( savedCategory );
	} catch ( err ) {
		return res.json( err );
	}
};

const getCategory = async( req, res )=>{

	const category = req.category;

	try {
		return res.json( category );
	} catch ( err ) {
		return res.json( err );
	}
};

const getCategories = async( req, res )=>{

	try {
		const categories = await Category.find();
		return res.json( categories );
	} catch ( err ) {
		return res.json( err );
	}
};

const updateCategory = async( req , res )=> {

	const category= req.category;

	try {
		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updateCategory = await Category.findByIdAndUpdate( category._id, updateData, { new :true } );
		return res.json( updateCategory );
	} catch ( err ) {
		return res.json( err );
	}
};

const deleteCategory = async( req, res )=>{
	const category = req.category;

	try {
		const deleteCategory = await Category.findOneAndRemove( category.category );
		return res.json( deleteCategory );
	} catch ( err ) {
		return res.json( err );
	}
};

module.exports.createCategory = createCategory;
module.exports.getCategory = getCategory;
module.exports.getCategories = getCategories;
module.exports.updateCategory = updateCategory;
module.exports.deleteCategory = deleteCategory;