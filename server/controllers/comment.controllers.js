const Comment = require( "../models/comment.models.js" );

const createComment = async( req, res )=>{

	const story = req.story;
	const comment = new Comment( {
		body: req.body.body,
		author: req.body.author,
		story: story._id
	} );
	try {
		const savedComment = await comment.save();
		await req.story.comments.push( comment );
		await req.story.save();
		return res.json( savedComment );
	} catch ( err ) {
		return res.json( { msg: err } );
	}
};

const getComments = async( req, res )=>{

	const story = req.story;
	try {
		const comments =await Comment.find( { story: story._id } ).populate( "author story" );
		return res.json( comments );
	} catch ( err ) {
		return res.json( { msg: err } );
	}
};

const updateComment = async( req, res )=>{

	const comment = req.comment;
	try {
		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updateComment = await Comment.findByIdAndUpdate( comment._id, updateData, { new: true } );
		return res.json( updateComment );
	} catch ( err ) {
		return res.json( { msg: err } );
	}
};

const deleteComment = async( req, res )=>{
	const comment = req.comment;

	try {
		const deleteComment = await Comment.findByIdAndDelete( comment._id );
		return res.json( deleteComment );
	} catch ( err ) {
		return res.json( { msg: err } );
	}
};

module.exports.createComment = createComment;
module.exports.getComments = getComments;
module.exports.updateComment = updateComment;
module.exports.deleteComment =deleteComment;