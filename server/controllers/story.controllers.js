const Story = require( "../models/story.models" );

const createStory = async ( req, res )=>{

	const story = new Story( {
		title: req.body.title,
		body: req.body.body,
		description: req.body.description,
		author: req.body.author,
		// tagList: req.body.tagList,
		// storyImage: req.file.filename
	} );

	try {
		const savedStory = await story.save();
		return res.json( savedStory );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

//Get all stories
const getStories = async ( req, res ) => {
	try{
		const stories = await Story.find().populate( "author comments tagList category" );
		return res.json( stories );
	}catch( err ){
		throw res.status( 500 ).json( { err_message: err } );
	}
};
//Get single story
const getStory = async( req, res )=> {

	const story = req.story;

	try{
		return res.json( story );

	}catch( err ){
		res.status( 500 ).json( { err_message: err } );
	}
};
//update story
const updateStory = async( req, res )=>{

	const story = req.story;
	try {

		const dataToUpdate = req.body;
		let { ...updateData } = dataToUpdate;
		if( req.file !== undefined ){
			updateData = { ...updateData, storyImage:req.file.filename };
		}
		const updateStory = await Story.findByIdAndUpdate( story._id, updateData, { new: true } );
		await updateStory.calculateReadingTime();
		await updateStory.toSanitizedHtml();
		await updateStory.save();
		return res.json( updateStory );
	} catch ( err ) {
		res.status( 500 ).json( { err_message: err } );
	}
};
//Get delete story
const deleteStory = async( req, res )=>{
	try {
		const story = req.story;
		const deleteStory = await Story.findByIdAndDelete( story._id );
		return res.json( deleteStory );
	} catch ( err ) {
		res.status( 500 ).json( { err_message: err } );
	}
};

module.exports.createStory = createStory;
module.exports.getStory = getStory;
module.exports.getStories = getStories;
module.exports.updateStory = updateStory;
module.exports.deleteStory = deleteStory;