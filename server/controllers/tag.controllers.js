const Tag = require( "../models/tag.models" );

const createTag = async( req, res )=>{

	const tag = new Tag( {
		tag: req.body.tag,
	} );

	try {
		const savedTag = await tag.save();
		return res.json( savedTag );
	} catch ( err ) {
		return res.json( err );
	}
};

const getTag = async( req, res )=>{

	const tag = req.tag;

	try {
		return res.json( tag );
	} catch ( err ) {
		return res.json( err );
	}
};

const getTags = async( req, res )=>{

	try {
		const tags = await Tag.find();
		return res.json( tags );
	} catch ( err ) {
		return res.json( err );
	}
};

const updateTag = async( req , res )=> {

	const tag= req.tag;

	try {
		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updateTag = await Tag.findByIdAndUpdate( tag._id, updateData, { new :true } );
		return res.json( updateTag );
	} catch ( err ) {
		return res.json( err );
	}
};

const deleteTag = async( req, res )=>{
	const tag = req.tag;

	try {
		const deleteTag = await Tag.findOneAndRemove( tag.tag );
		return res.json( deleteTag );
	} catch ( err ) {
		return res.json( err );
	}
};

module.exports.createTag = createTag;
module.exports.getTag = getTag;
module.exports.getTags = getTags;
module.exports.updateTag = updateTag;
module.exports.deleteTag = deleteTag;