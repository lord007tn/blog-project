const User = require( "../models/user.models" );

const currentUser = async( req, res ) => {

	try {
		const user = await User.findById( req.verifiedUser._id );
		return res.json( user.ownerProfile() );
	} catch ( err ) {
		return res.status( 401 ).json( err );
	}
};

const getUsers = async( req, res )=>{

	try {
		const users = await User.find();
		users.forEach( ( user )=>{return res.json( user.authToJSON() );} );
	} catch ( err ) {
		return res.json( err );
	}
};
const getUser = async( req, res )=>{
	try {
		return res.json( req.user );
	} catch ( err ) {
		return res.json( err );
	}
};

const updateUser = async( req, res )=>{
	const id = req.user._id;
	try {
		const dataToUpdate = req.body;
		let { ...updateData } = dataToUpdate;
		if( req.file !== undefined ){
			updateData = { ...updateData, avatar:req.file.filename };
		}
		const updateUser = await User.findByIdAndUpdate( id, updateData, { new :true } );
		return res.json( updateUser );
	} catch ( err ) {
		return res.json( err );
	}
};

const deleteUser = async( req, res )=>{
	const id = req.user._id;

	try {
		const deleteUser = await User.findByIdAndDelete( id );
		return res.json( deleteUser );
	} catch ( err ) {
		return res.json( err );
	}
};
module.exports.currentUser = currentUser;
module.exports.getUser = getUser;
module.exports.getUsers = getUsers;
module.exports.updateUser = updateUser;
module.exports.deleteUser = deleteUser;