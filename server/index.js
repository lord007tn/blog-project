const express = require( "express" );
const app = express();
const dotenv = require( "dotenv" );
const mongoose = require( "mongoose" );
const bodyparser = require( "body-parser" );
const cors = require( "cors" );
const path = require( "path" );
const Grid = require( "gridfs-stream" );
//cors Config
// const corsOptions = {
// 	origin: process.env.CORS_ORIGIN || "http://localhost:3000",
// 	optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
// 	credentials: true
// };
//Dotenv Config
dotenv.config();
//Connect to database
mongoose.set( "useNewUrlParser", true );
mongoose.set( "useFindAndModify", false );
mongoose.set( "useCreateIndex", true );
mongoose.set( "useUnifiedTopology", true );
mongoose.connect(process.env.MONGO_URI);
mongoose.connection.on( "connected", () => {console.log( "Connected" );} );
mongoose.connection.on( "error", ( err ) => console.log( "Connection failed with - ",err ) );
//Grid FS
let gfs;

mongoose.connection.once( "open", function () {
	gfs = Grid( mongoose.connection.db, mongoose.mongo );
	gfs.collection( "uploads" );
} );
// Image route
app.get( "/image/:filename", ( req, res ) => {
	gfs.files.findOne( { filename: req.params.filename }, ( err, file ) => {
	// Check if file
		if ( !file || file.length === 0 ) {
			return res.status( 404 ).json( { msg: "No file exists" } );
		}
		// Check if image
		if ( file.contentType === "image/jpeg" || file.contentType === "image/png" ) {
		// Read output to browser
			const readstream = gfs.createReadStream( file.filename );
			readstream.pipe( res );
		} else {
			res.status( 404 ).json( { msg: "Not an image" } );
		}
	} );
} );
//Importe routes
const storyRoute = require( "./routes/story.routes" );
const authRoute = require( "./routes/user.auth.routes" );
const tagRoute = require( "./routes/tag.routes" );
const userRoute = require( "./routes/user.routes" );
const categoryRoute = require( "./routes/category.routes" );
//Middleware
app.use( bodyparser.json() );
app.use( bodyparser.urlencoded( { extended : false } ) );
app.use( cors() );

// // Serve static assets if in production
// if( process.env.NODE_ENV === "production" ){
// 	app.use( express.static( "/client/build" ) );

// 	app.use( "*", ( req, res ) => {
// 		res.sendFile( path.resolve( "client", "build", "index.html" ) );
// 	} );
	
// }

//Route Middleware
app.use( "/category", categoryRoute );
app.use( "/story", storyRoute );
app.use( "/", authRoute );
app.use( "/tags", tagRoute );
app.use( "/users", userRoute );
//Server Run
const port = process.env.PORT || 8000;
app.listen( port, () => {
	console.log( "Server is up and running on port number " + port );
} );
