const mongoose = require( "mongoose" );
const uniqueValidator = require( "mongoose-unique-validator" );
const Schema = mongoose.Schema;

const CategorySchema = new Schema( {
	category: { type: String, lowercase: true, unique: true },
	color: { type: String, unique:true, lowercase:true, max: 64 },
	about: { type: String, maxlength: 512 }

},{ timestamps: true } );

CategorySchema.plugin( uniqueValidator, { message: "is already taken" } );

module.exports = mongoose.model( "Category", CategorySchema ) || mongoose.models.Category;