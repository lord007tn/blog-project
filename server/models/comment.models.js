const mongoose = require( "mongoose" );

const CommentSchema = new mongoose.Schema( {
	body: { type: String, required: true },
	author: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
	story: { type: mongoose.Schema.Types.ObjectId, ref: "Story" },
	// reply: [ { type: mongoose.Schema.Types.ObjectId, ref: "Comment" } ]
}, { timestamps: true } );

module.exports = mongoose.model( "Comment", CommentSchema ) || mongoose.models.Comment;