const mongoose = require( "mongoose" );
const uniqueValidator = require( "mongoose-unique-validator" );
const Schema = mongoose.Schema;

const RegistrationCodeSchema = new Schema( {
	email: { type: String, required:[ true, "can't be blank" ], index:true, lowercase: true, unique: true },
	registrationCode: { type: String, required:[ true, "can't be blank" ], index:true, unique: true, },
	isAccepted: { type: Boolean, default: false }
}, { timestamps: true } );

RegistrationCodeSchema.plugin( uniqueValidator, { message: "is already taken." } );

RegistrationCodeSchema.pre( "validate", function( next ){
	if( !this.slug )  {
		this.registrationCodeGenerator();
	}
	next();
} );

RegistrationCodeSchema.methods.registrationCodeGenerator = function(){
	this.registrationCode = ( Math.random() * Math.pow( 36, 6 ) | 0 ).toString( 36 );
};
module.exports = mongoose.model( "RegistrationCode", RegistrationCodeSchema ) || mongoose.models.RegistrationCode;