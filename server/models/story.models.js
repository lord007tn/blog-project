const User = require( "./user.models" );
const mongoose = require( "mongoose" );
const uniqueValidator = require( "mongoose-unique-validator" );
const slug = require( "slug" );

const marked = require( "marked" );
const createDomPurify = require( "dompurify" );
const { JSDOM } = require( "jsdom" );
const dompurify = createDomPurify( new JSDOM().window );

const Schema = mongoose.Schema;


const StorySchema = new Schema( {
	slug: { type: String, lowercase: true, unique: true },
	title: { type: String, required:true },
	description: { type: String, required:true },
	body: { type: String, required:true },
	sanitizedHtml: { type: String, required:true },
	lovesCount: { type: Number, default: 0 },
	comments: [ { type: mongoose.Schema.Types.ObjectId, ref: "Comment" } ],
	tagList: [ { type: mongoose.Schema.Types.ObjectId, ref: "Tag" } ],
	author: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
	category: [ { type: mongoose.Schema.Types.ObjectId, ref: "Category" } ],
	storyImage: { type: String, default:" " },
	readingTime: { type:Number, default: 1 }
}, { timestamps: true } );

StorySchema.plugin( uniqueValidator, { message: "is already taken" } );

StorySchema.pre( "validate", function( next ){
	if( !this.slug )  {
		this.slugify();
	}
	if( this.body ) {
		this.toSanitizedHtml();
	}
	next();
} );
StorySchema.pre( "save", function( next ){

	this.calculateReadingTime();
	next();
} );

StorySchema.methods.slugify = function() {
	this.slug = slug( this.title ) + "-" + ( Math.random() * Math.pow( 36, 6 ) | 0 ).toString( 36 );
};

StorySchema.method.updateLovesCount = function (){
	const story = this;
	const count = User.count( { favorites: { $in: [ story._id ] } } );
	story.lovesCount = count;
	return story.save();
};

StorySchema.methods.storyToJSON = function( user ){
	return {
		slug: this.slug,
		title: this.title,
		description: this.description,
		body: this.body,
		createdAt: this.createdAt,
		updatedAt: this.updatedAt,
		tagList: this.tagList,
		lovesCount: this.lovesCount,
		author: this.author.profileToJSON( user )
	};
};

StorySchema.methods.calculateReadingTime = function(){
	const wordsPerMinute = 200;
	const noOfWords = this.body.split( /\s/g ).length;
	const minutes = noOfWords / wordsPerMinute;
	const readTime = Math.ceil( minutes );
	return this.readingTime = readTime;
};

StorySchema.methods.toSanitizedHtml = function(){
	this.sanitizedHtml = dompurify.sanitize( marked( this.body ) );
};
module.exports = mongoose.model( "Story", StorySchema ) || mongoose.models.Story;

