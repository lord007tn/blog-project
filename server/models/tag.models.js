const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const TagSchema = new Schema( {
	tag: { type: String, lowercase: true, unique: true },

},{ timestamps: true } );

module.exports = mongoose.model( "Tag", TagSchema ) || mongoose.models.Tag;