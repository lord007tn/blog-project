const mongoose = require( "mongoose" );
const uniqueValidator = require( "mongoose-unique-validator" );
const Schema = mongoose.Schema;

const UserSchema = new Schema( {
	userName: { type: String, unique: true, required: true, min: 6, max: 128 },
	firstName: { type: String, max: 64 },
	lastName: { type:String, max: 64 },
	email: { type: String, required:[ true, "can't be blank" ], index:true, lowercase: true, unique: true, },
	password: { type: String, required: [ true, "can't be blank" ], max: 1024 },
	bio: { type: String, max:512 },
	avatar: { type: String },
	following: [ { type: mongoose.Schema.Types.ObjectId, ref: "User" } ],
	lovedStories: [ { type: mongoose.Schema.Types.ObjectId, ref: "Story" } ],
	isAdmin: { type: Boolean, default:false },
	isAuthor: { type: Boolean, default: false }
}, { timestamps: true } );

UserSchema.plugin( uniqueValidator, { message: "is already taken." } );

UserSchema.methods.love = function( id ){
	if( this.lovedStories.indexOf( id ) === -1 ){

		this.lovedStories.push( id );
	}
	return this.save();
};

UserSchema.methods.unlove = function( id ){
	this.lovedStories.remove( id );
	return this.save();
};

UserSchema.methods.follow = function( id ){
	if( this.following.indexOf( id ) === -1 ){
		this.following.push( id );
	}

	return this.save();
};

UserSchema.methods.unfollow = function( id ){
	this.following.remove( id );
	return this.save();
};
UserSchema.methods.profileToJSON = function(){

	return {
		_id: this._id,
		username: this.username,
		firstName: this.firstName,
		lastName: this.lastName,
		bio: this.bio,
		avatar: this.avatar
	};
};


UserSchema.methods.authToJSON = function(){
	return {
		_id: this._id,
		username: this.username,
		email: this.email,
		bio: this.bio,
		avatar: this.avatar
	};
};

UserSchema.methods.ownerProfile = function(){
	return{
		username: this.username,
		firstName: this.firstName,
		lastName: this.lastName,
		email: this.email,
		bio: this.bio,
		following: this.following,
		lovedStories: this.lovedStories,
		avatar: this.avatar
	};
};
module.exports = mongoose.model( "User", UserSchema ) || mongoose.models.User;