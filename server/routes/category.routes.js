const router = require( "express" ).Router();
const Category = require( "../models/category.models" );
const categoryControllers = require( "../controllers/category.controllers" );
const verifyToken = require( "../utils/verifyToken" );
router.param( "category", async( req, res,next, name )=>{
	try{
		const category = await Category.findOne( { category: name } );
		if( !category ) return res.sendStatus( 404 );
		req.category = category;
		return next();

	}catch( err ){
		res.status( 500 ).json( { msg : err } );
	}

} );

router.get( "/categories", categoryControllers.getCategories );
router.get( "/:category", categoryControllers.getCategory );
router.post( "/create", verifyToken, categoryControllers.createCategory );
router.put( "/:category/update", verifyToken, categoryControllers.updateCategory );
router.delete( "/:category/delete", verifyToken, categoryControllers.deleteCategory );
module.exports = router;