const router = require( "express" ).Router();
const storyControllers = require( "../controllers/story.controllers" );
const commentsControllers = require( "../controllers/comment.controllers" );
const Story = require( "../models/story.models" );
const Comment = require( "../models/comment.models" );

const verifyToken = require( "../utils/verifyToken" );
const multer = require( "multer" );
const GridFsStorage = require( "multer-gridfs-storage" );
const crypto = require( "crypto" );
const path = require( "path" );
const storage = new GridFsStorage( {
	url: process.env.MONGO_URI,
	file: ( req, file ) => {
		return new Promise( ( resolve, reject ) => {
			crypto.randomBytes( 16, ( err, buf ) => {
				if ( err ) {
					return reject( err );
				}
				const filename = buf.toString( "hex" ) + path.extname( file.originalname );
				const fileInfo = {
					filename: filename,
					bucketName: "uploads"
				};

				resolve( fileInfo );
			} );
		} );
	}
} );
const upload = multer( { storage } );
router.param( "story", async( req, res,next, slug )=>{
	try{
		const story = await Story.findOne( { slug: slug } ).populate( "author comments tagList category" );
		if( !story ) return res.sendStatus( 404 );
		req.story = story;
		return next();

	}catch( err ){
		res.status( 500 ).json( { msg : err } );
	}

} );


// preload story object on routes with ':comment'
router.param( "comment", async( req, res,next, id )=>{
	try{
		const comment = await  Comment.findById( id ).populate( "author story" );
		if( !comment ) return res.sendStatus( 404 );
		req.comment = comment;
		return next();

	}catch( err ){
		res.status( 500 ).json( { msg : err } );
	}

} );
// stories routing
router.get( "/stories", storyControllers.getStories );

router.get( "/:story", storyControllers.getStory );

router.post( "/create",upload.single( "storyImage" ), verifyToken, storyControllers.createStory );

router.put( "/:story/update",upload.single( "storyImage" ), verifyToken, storyControllers.updateStory );

router.delete( "/:story/delete", verifyToken, storyControllers.deleteStory );

// comments routing
router.get( "/:story/comments", commentsControllers.getComments );

router.post( "/:story/comment/create", verifyToken, commentsControllers.createComment );

router.put( "/:story/comment/:comment/update", verifyToken, commentsControllers.updateComment );

router.delete( "/:story/comment/:comment/delete", verifyToken, commentsControllers.deleteComment );

module.exports = router;