const router = require( "express" ).Router();
const tagControllers = require( "../controllers/tag.controllers" );
const Tag = require( "../models/tag.models" );
const verifyToken = require( "../utils/verifyToken" );
router.param( "tag", async( req, res,next, name )=>{
	try{
		const tag = await Tag.findOne( { tag: name } );
		if( !tag ) return res.sendStatus( 404 );
		req.tag = tag;
		return next();

	}catch( err ){
		res.status( 500 ).json( { msg : err } );
	}

} );
router.get( "/:tag", tagControllers.getTag );
router.get( "/", tagControllers.getTags );
router.post( "/create", verifyToken, tagControllers.createTag );
router.put( "/:tag/update", verifyToken, tagControllers.updateTag );
router.delete( "/:tag/delete", verifyToken, tagControllers.deleteTag );

module.exports = router;