
const dotenv = require( "dotenv" );
const router = require( "express" ).Router();
const jwt = require( "jsonwebtoken" );
const bcrypt = require( "bcryptjs" );
const User = require( "../models/user.models" );

const verifyToken = require( "../utils/verifyToken" );
router.get( "/authcheck", verifyToken, async ( req, res )=>{
	try {
		const user = await User.findById( req.verifiedUser._id );
		res.json( user.authToJSON() );
	} catch ( err ) {
		res.status( 500 ).json( "Server error" );
	}
} );
router.post( "/register", async ( req, res )=> {
	//Validating the data we parse in body
	//Checking if the email is validation
	const existEmail = await User.findOne( { email: req.body.email } );
	if ( existEmail ) return res.status( 400 ).json( "Email already exist" );

	//Hashing the password
	const salt = await bcrypt.genSalt( 16 );
	const hashedPassword =await bcrypt.hash( req.body.password, salt );
	//Creating new user
	const user = new User( {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		userName: req.body.userName,
		email: req.body.email,
		password: hashedPassword
	} );
	try{
		await user.save();
		res.json( { user: user._id } );
	}catch( err ){
		res.status( 400 ).json( err );
	}
} );
router.post( "/login", async ( req, res )=> {
	//Validating the data
	//Checking if email is valid
	const user = await User.findOne( { email: req.body.email } );
	if ( !user ) return res.status( 400 ).json( "Wrong Email or Password" );
	//Validate password
	const validPass = await bcrypt.compare( req.body.password, user.password );
	if ( !validPass ) return res.status( 400 ).json( "Wrong Email or Password" );
	//Generating Token
	const token = jwt.sign( { _id: user._id }, process.env.TOKEN_KEY, { expiresIn: "2 days" } );
	res.header( "access_token", token ).json( { message: "login valid", token: token, user: user.authToJSON() } );
} );
module.exports = router;
