const router = require( "express" ).Router();
const userControllers = require( "../controllers/user.controllers" );
const User = require( "../models/user.models" );
const verifyToken = require( "../utils/verifyToken" );

const multer = require( "multer" );
const GridFsStorage = require( "multer-gridfs-storage" );
const crypto = require( "crypto" );
const path = require( "path" );
const storage = new GridFsStorage( {
	url: process.env.MONGO_URI,
	file: ( req, file ) => {
		return new Promise( ( resolve, reject ) => {
			crypto.randomBytes( 16, ( err, buf ) => {
				if ( err ) {
					return reject( err );
				}
				const filename = buf.toString( "hex" ) + path.extname( file.originalname );
				const fileInfo = {
					filename: filename,
					bucketName: "uploads"
				};

				resolve( fileInfo );
			} );
		} );
	}
} );
const upload = multer( { storage } );

router.param( "user", async( req, res,next, id )=>{
	try{
		const user = await User.findById( id );
		if( !user ) return res.sendStatus( 404 );
		req.user = user;
		return next();

	}catch( err ){
		res.status( 500 ).json( { msg : err } );
	}

} );
router.get( "/currentuser", verifyToken, userControllers.currentUser );
router.get( "/:user", verifyToken, userControllers.getUser );
router.get( "/", verifyToken, userControllers.getUsers );
router.put( "/:user/update",upload.single( "avatar" ), verifyToken, userControllers.updateUser );
router.delete( "/:user/delete", verifyToken, userControllers.deleteUser );

module.exports = router;